﻿using System.Security.Principal;
using System;
using System.Linq;
using System.Dynamic;
using System.Collections.Generic;

namespace prog
{
    public record Student(string name,int group,string exam,double mark);
    public class Class1

    {
        public static IEnumerable<string> StudentsHighestScore(List<Student> examinationList) {
    
      var rez = examinationList
       .GroupBy(
        student => student.name,
        student => student.mark,
        (strudentName, studentMarks) => new {
           Key = strudentName,
           AverageMarks = studentMarks.Average(),                   
        }    
    )  .GroupBy(
       stud =>  Math.Round(stud.AverageMarks,2),
      stud =>  stud.Key,
      (AverageMarks, Names) => new {
      Key=AverageMarks,
        NamesArr=Names                   
      } ).OrderByDescending(student => student.Key).First().NamesArr;

      return rez;
        }

    public static Dictionary<string,double> StudentsHighestScoreSubj(List<Student> examinationList) {

      var rez = examinationList
   
     .GroupBy(
     student => student.exam,
     student => student.mark,
      (strudentExams, studentMarks) => new {
           Key = strudentExams,
           AverageMarks = Math.Round(studentMarks.Average(),2),          
       }    
        
   ) 
   .ToDictionary(s => s.Key, s => s.AverageMarks);

      return rez; 
    }

    public static Dictionary<string,List<int>> StudentsHighestScoreSubjGroup(List<Student> examinationList) {

      var rez = examinationList
   
     .GroupBy(
      student => new { Exam = student.exam, Group = student.group},
      student => student.mark,
      (ExamsAndGroups, studentMarks) => new {
           Key = new { Exam = ExamsAndGroups.Exam, Group = ExamsAndGroups.Group},
           AverageMarks = Math.Round(studentMarks.Average(),2),          
       }    
     )
    .GroupBy(
      student => new { Mark = student.AverageMarks, Exam = student.Key.Exam},
      student => student.Key.Group,
      (MarksAndExams, studentKeyGroup) => new {
           Exam = MarksAndExams.Exam,
           KeyGroupMarksExams = new {studentKeyGroup, MarksAndExams.Mark}          
       }    
     ).GroupBy(
      student => student.Exam,
      student => student.KeyGroupMarksExams,
      (studentExams, studentKeyGroupMarksExams) => new {
           Exam = studentExams,
           Best = studentKeyGroupMarksExams.OrderByDescending(s => s.Mark).First().studentKeyGroup      
       }    
      ) .ToDictionary(s => s.Exam, s => s.Best.ToList<int>());

      return rez; 
    }
    
    }


}
