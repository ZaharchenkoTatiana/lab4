using System.Reflection;
using System;
using Xunit;
using System.Collections.Generic;


namespace prog.tests
{
    public class UnitTest1
    {
        [Fact]
        public void StudentWithHighestScore()
        {
            List<Student> table = new List<Student>{ 
            new Student ( "Name1", 1,"Algebra",2),
            new Student ( "Name1", 1,"English",3),
            new Student ( "Name2", 3,"Programming",5),
            new Student ( "Name2", 3,"English",5),
            new Student ( "Name3", 2,"Programming",4),
            new Student ( "Name3", 2,"Algebra",4),
            new Student ( "Name3", 2,"English",5),
            new Student ( "Name4", 3,"Programming",4),
            new Student ( "Name5", 2,"English",2),
            new Student ( "Name5", 2,"Algebra",3),
            new Student ( "Name6", 1,"Algebra",2),
            new Student ( "Name6", 1,"English",4),
            new Student ( "Name6", 1,"Programming",3)
            };
            Assert.Equal(new List<string> {"Name2"}, Class1.StudentsHighestScore(table));        
    }
        [Fact]
    public void StudentsWithHighestScore()
        {
            
            List<Student> table = new List<Student>{ 
            new Student ("Name1", 1,"Algebra",2),
            new Student ( "Name1", 1,"English",3),
            new Student ( "Name2", 3,"Programming",5),
            new Student ( "Name2", 3,"English",5),
            new Student ( "Name3", 2,"Programming",4),
            new Student ( "Name3", 2,"Algebra",4),
            new Student ( "Name3", 2,"English",5),
            new Student ( "Name4", 3,"Programming",4),
            new Student ( "Name5", 2,"English",2),
            new Student ( "Name5", 2,"Algebra",3),
            new Student ( "Name6", 1,"Algebra",5),
            new Student ( "Name6", 1,"English",5),
            new Student ( "Name6", 1,"Programming",5)
            };
            Assert.Equal(new List<string>{"Name2","Name6"}, Class1.StudentsHighestScore(table));        
    }
    [Fact]
    public void AverageScoreInEachSubjecte()
        {
            
            List<Student> table = new List<Student>{ 
            new Student ("Name1", 1,"Algebra",2),
            new Student ( "Name1", 1,"English",3),
            new Student ( "Name2", 3,"Programming",5),
            new Student ( "Name2", 3,"English",5),
            new Student ( "Name3", 2,"Programming",4),
            new Student ( "Name3", 2,"Algebra",4),
            new Student ( "Name3", 2,"English",5),
            new Student ( "Name4", 3,"Programming",4),
            new Student ( "Name5", 2,"English",2),
            new Student ( "Name5", 2,"Algebra",3),
            new Student ( "Name6", 1,"Algebra",5),
            new Student ( "Name6", 1,"English",5),
            new Student ( "Name6", 1,"Programming",5)
            };
            Dictionary<string, double> CorRes = new Dictionary<string, double>(){
                {"Algebra",3.5},
                {"Programming",4.5},
                {"English",4}
            };
            
            var rez = Class1.StudentsHighestScoreSubj(table);
           Assert.Equal(CorRes, rez); 
        
        }

        [Fact]
    public void AverageScoreInEachSubjecte2()
        {
            
            List<Student> table = new List<Student>{ 
            new Student ("Name1", 1,"Algebra",5),
            new Student ( "Name1", 1,"English",3),
            new Student ( "Name2", 3,"Programming",5),
            new Student ( "Name2", 3,"English",4),
            new Student ( "Name3", 2,"Programming",5),
            new Student ( "Name3", 2,"Algebra",2),
            new Student ( "Name3", 2,"English",5),
            new Student ( "Name4", 3,"Programming",2),
            new Student ( "Name5", 2,"English",4),
            new Student ( "Name5", 2,"Algebra",5),
            new Student ( "Name6", 1,"Algebra",5),
            new Student ( "Name6", 1,"English",5),
            new Student ( "Name6", 1,"Programming",5)
            };
            Dictionary<string, double> CorRes = new Dictionary<string, double>(){
                {"Algebra",4.25},
                {"Programming",4.25},
                {"English",4.2}
            };
            
            var rez = Class1.StudentsHighestScoreSubj(table);
           Assert.Equal(CorRes, rez); 
        
        }

    [Fact]
    public void AverageScoreInEachSubjecteGroup()
        {
            
            List<Student> table = new List<Student>{ 
            new Student ("Name1", 1,"Algebra",2),
            new Student ( "Name1", 1,"English",3),
            new Student ( "Name2", 3,"Programming",5),
            new Student ( "Name2", 3,"English",5),
            new Student ( "Name3", 2,"Programming",4),
            new Student ( "Name3", 2,"Algebra",4),
            new Student ( "Name3", 2,"English",5),
            new Student ( "Name4", 3,"Programming",4),
            new Student ( "Name5", 2,"English",2),
            new Student ( "Name5", 2,"Algebra",3),
            new Student ( "Name6", 1,"Algebra",5),
            new Student ( "Name6", 1,"English",5),
            new Student ( "Name6", 1,"Programming",5)
            };
            Dictionary<string, List<int>> CorRes = new Dictionary<string, List<int>>(){
                {"Algebra", new List<int>{1,2}},
                {"Programming",new List<int>{1}},
                {"English",new List<int>{3}}
            };
            
            var rez = Class1.StudentsHighestScoreSubjGroup(table);
           Assert.Equal(CorRes, rez); 
        
        }
        [Fact]
    public void AverageScoreInEachSubjecteGroup2()
        {
            
            List<Student> table = new List<Student>{ 
            new Student ("Name1", 1,"Algebra",2),
            new Student ( "Name1", 1,"English",3),
            new Student ( "Name2", 3,"Programming",5),
            new Student ( "Name2", 3,"English",3),
            new Student ( "Name3", 2,"Programming",2),
            new Student ( "Name3", 2,"Algebra",4),
            new Student ( "Name3", 2,"English",5),
            new Student ( "Name4", 3,"Programming",4),
            new Student ( "Name5", 2,"English",5),
            new Student ( "Name5", 2,"Algebra",3),
            new Student ( "Name6", 1,"Algebra",4),
            new Student ( "Name6", 1,"English",5),
            new Student ( "Name6", 1,"Programming",3)
            };
            Dictionary<string, List<int>> CorRes = new Dictionary<string, List<int>>(){
                {"Algebra", new List<int>{2}},
                {"Programming",new List<int>{3}},
                {"English",new List<int>{2}}
            };
            
           var rez = Class1.StudentsHighestScoreSubjGroup(table);
           Assert.Equal(CorRes, rez); 
        
        }   
    }
}



